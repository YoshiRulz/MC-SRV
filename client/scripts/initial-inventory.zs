mods.initialinventory.InvHandler.addStartingItem(<antiqueatlas:empty_antique_atlas>, 7);
mods.initialinventory.InvHandler.addStartingItem(<akashictome:tome>.withTag({
		"akashictome:is_morphing": 1 as byte,
		"akashictome:data": {
			abyssalcraft: {id: "abyssalcraft:necronomicon", Count: 1 as byte, tag: {"akashictome:definedMod": "abyssalcraft", PotEnergy: 0.0 as float}, Damage: 0 as short},
			astralsorcery: {id: "astralsorcery:itemjournal", Count: 1 as byte, tag: {"akashictome:definedMod": "astralsorcery"}, Damage: 0 as short},
			thebetweenlands: {id: "thebetweenlands:manual_hl", Count: 1 as byte, tag: {"akashictome:definedMod": "thebetweenlands"}, Damage: 0 as short},
			botania: {id: "botania:lexicon", Count: 1 as byte, tag: {"akashictome:definedMod": "botania"}, Damage: 0 as short},
			guideapi: {id: "guideapi:bloodmagic-guide", Count: 1 as byte, tag: {"akashictome:definedMod": "guideapi", "G-API_Category_Page": 0}, Damage: 0 as short},
			guideapi1: {id: "guideapi:modularrouters-guidebook", Count: 1 as byte, tag: {"akashictome:definedMod": "guideapi", "G-API_Category_Page": 0}, Damage: 0 as short},
			guideapi2: {id: "guideapi:woot-guide", Count: 1 as byte, tag: {"akashictome:definedMod": "guideapi", "G-API_Category_Page": 0}, Damage: 0 as short},
			extrautils2: {id: "extrautils2:book", Count: 1 as byte, tag: {"akashictome:definedMod": "extrautils2"}, Damage: 0 as short},
			immersiveengineering: {id: "immersiveengineering:tool", Count: 1 as byte, tag: {"akashictome:definedMod": "immersiveengineering"}, Damage: 3 as short},
			industrialforegoing: {id: "industrialforegoing:book_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "industrialforegoing"}, Damage: 0 as short},
			roots: {id: "roots:ritual_book", Count: 1 as byte, tag: {"akashictome:definedMod": "roots"}, Damage: 0 as short},
			roots1: {id: "roots:spellcraft_book", Count: 1 as byte, tag: {"akashictome:definedMod": "roots1"}, Damage: 0 as short},
			roots2: {id: "roots:herblore_book", Count: 1 as byte, tag: {"akashictome:definedMod": "roots2"}, Damage: 0 as short},
			thaumcraft: {id: "thaumcraft:thaumonomicon", Count: 1 as byte, tag: {"akashictome:definedMod": "thaumcraft"}, Damage: 0 as short},
			totemic: {id: "totemic:totempedia", Count: 1 as byte, tag: {"akashictome:definedMod": "totemic"}, Damage: 0 as short}
		}
	}), 8);
